import Queue as q

# hacer arbol para recorrerlo

customers = {0:"P", 1:"Q", 2:"R", 3:"S"}
taxis = {0: "T1", 1: "T2", 2: "T3", 3: "T4"}
#
table = [[10, 8, 4, 6],
         [6, 4, 12, 8],
         [14, 10, 8, 2],
         [4, 14, 10, 9]]


# table = [[10, 8, 4, 6],
#          [6, 4, 2, 8],
#          [14, 10, 8, 2],
#          [4, 14, 10, 9]]

class Node:
    def __init__(self, parent, values, level):
        self.level = level #nivel en el arbol
        self.values = values # Conjunto de valores que conforman una posible estado {T1,T2,T3,T4}
        self.parent = parent
        self.children = []
        self.locationValues = [] #guarda la localizacion de los valores en la tabla (x, y)

        #calculo la heuristica
        self.h = 0
        for i in range(level, 4):
            self.h += min(table[i])

        #calculo la distacia acumulada desde la raiz
        if self.parent == None:
            self.g = 0
        else:
            self.g = sum(values)

        #Calculo de la
        self.f = self.g + self.h

    def toString(self):
        return "Nivel: " + str(self.level) + ", Valores: " + str(self.values)

#################### Creacion del Arbol ####################

def makeGraph():
    # T1, T2, T3, T4
    queue = q.Queue()
    root =  Node(None, [], 0)
    queue.put(root)

    print( "by Luis  A* |||||")

    while not queue.empty():
        currentNode = queue.get() # Toma el primer elemento y lo elimina de la fila.
        if( len(currentNode.values) <4 ):  # Si el nodo tiene menos de 4 variables
                                           # quiere decir que aun no es una hoja del arbol.
            for i in range(4): # Se crean 4 nuevos hijos para un nodo padre
                newLevel = currentNode.level + 1
                newValues = currentNode.values[:]
                newValues.append(table[newLevel - 1][i])
                newChild = Node(currentNode, newValues, newLevel)
                newChild.locationValues = currentNode.locationValues[:]
                newChild.locationValues.append([newLevel - 1, i])
                currentNode.children.append(newChild)
                queue.put(newChild)

    return root

# Verificacion de las restricciones
def checkRestrictions(location):

    for i in range(len(location)):
        for j in range(i+1,len(location)):
            if location[i][1] == location[j][1]:
                return False
    return True


###################  Algoritmo A*  ###################
def A_asterisk(root):
    queue = q.PriorityQueue() # Una priority Queue guarda tuplas del tipo (Valor de prioridad, elemento)
    queue.put((root.f, root))

    while not queue.empty():
        currentTuple = queue.get()  # Toma el primer elemento y lo elimina de la fila
        currentNode = currentTuple[1]

        print( "Nodo: " + str(currentNode.values) + ", Costo al Objetivo: " + str(currentNode.f))

        if (len(currentNode.children) > 0): # Si el nodo aun no es una hoja
            for i in range(len(currentNode.children)):
                queue.put((currentNode.children[i].f, currentNode.children[i]))

        else: # El nodo es una Hoja y por lo tanto es una posible solucion
            if(checkRestrictions(currentNode.locationValues)):

                print ("-----  Solucion con A*    ----")

                for i in range(len(currentNode.locationValues)):
                    print( str(taxis[i]) , " = " , str(customers[currentNode.locationValues[i][1]]), " (", currentNode.values[i] ,")")

                break
            else:
                line = ""
                print ("Incumplimiento de las restricciones")
                for i in range(len(currentNode.locationValues)):
                    line += str(taxis[i]) + " = " + str(customers[currentNode.locationValues[i][1]]) + " (" + str(currentNode.values[i]) + "), "
                print (line)

root = makeGraph()
A_asterisk(root)
