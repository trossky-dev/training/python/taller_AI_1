import queue as q

table = [[10, 8, 4, 6],
         [6, 4, 2, 8],
         [14, 10, 8, 2],
         [4, 14, 10, 9]]

customers = {0:"P", 1:"Q", 2:"R", 3:"S"}
taxis = {0: "T1", 1: "T2", 2: "T3", 3: "T4"}

class Node:
    def __init__(self, parent, values, level):
        self.level = level #nivel en el arbol
        self.values = values # Conjunto de valores que conforman una posible estado {T1,T2,T3,T4}
        self.parent = parent
        self.children = []
        self.locationValues = [] #guarda la localizacion de los valores en la tabla (x, y)

        #calculo la heuristica
        self.h = 0
        for i in range(level, 4):
            self.h += min(table[i])

        #calculo la distacia acumulada desde la raiz
        if self.parent == None:
            self.g = 0
        else:
            self.g = sum(values)

        #Calculo de la
        self.f = self.g + self.h

def makeGraph():
    # T1, T2, T3, T4
    queue = q.Queue()
    root =  Node(None, [], 0)
    queue.put(root)

    while not queue.empty():
        #Toma el primer elemento y lo elimina de la fila
        currentNode = queue.get()

        #Si el nodo tiene menos de 4 variables quiere decir que aun no es una hoja del arbol
        if( len(currentNode.values) <4 ):
            for i in range(4): # Se crean 4 nuevos hijos para un nodo padre
                newLevel = currentNode.level + 1
                newValues = currentNode.values[:]
                newValues.append(table[newLevel - 1][i])
                newChild = Node(currentNode, newValues, newLevel)
                newChild.locationValues = currentNode.locationValues[:]
                newChild.locationValues.append([newLevel - 1, i])
                currentNode.children.append(newChild)
                queue.put(newChild)

    return root

def checkRestrictions(node):

    for i in range(len(node.locationValues)):
        for j in range(i+1,len(node.locationValues)):
            if node.locationValues[i][1] == node.locationValues[j][1]:
                return False
    return True



def DFSAlg(root):
    queue = q.LifoQueue()
    queue.put(root)

    while not queue.empty():
        currentNode = queue.get() # Toma el ultimo elemento y lo elimina de la Cola


        if (checkRestrictions(currentNode)):

            if (len(currentNode.children) > 0):  # Si el nodo aun no es una hoja

                for i in range(len(currentNode.children)):
                    queue.put(currentNode.children[i])

            else:  # El nodo es una Hoja y por lo tanto es una posible solucion

                print( " \t \t \t SOLUCION:")

                for i in range(len(currentNode.locationValues)):
                    print( "El Taxi",str(taxis[i]) , " se le asigna el cliente" , str(customers[currentNode.locationValues[i][1]]), " que esta a una distancia[", currentNode.values[i],"]" )

                break

if __name__ == '__main__':
    root = makeGraph()
    DFSAlg(root)
