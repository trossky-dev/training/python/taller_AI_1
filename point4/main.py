import Queue as q
import tree



def DFSAlg(root):
    queue = q.LifoQueue()
    queue.put(root)

    while not queue.empty():
        currentNode = queue.get()  # Toma el ultimo elemento y lo elimina de la Cola
        if (checkRestrictions(currentNode)):
            if (len(currentNode.children) > 0):  # Si el nodo aun no es una hoja
                for i in range(len(currentNode.children)):
                    queue.put(currentNode.children[i])

            else:  # El nodo es una Hoja y por lo tanto es una posible solucion
                print( "SOLUCION:")
                for i in range(len(currentNode.values)):
                    print( "T" + str(i+1) + "=  " + "P" + str(currentNode.values[i]))
                break


def checkRestrictions(currentNode):

    if(len(currentNode.values)==0):
        return True
    if (currentNode.maxTime() > int(tree.input.deadline)):
        print ("Failed: tasks time out of range of  Deadline")
        return False

    i =len(currentNode.values)-1

    if(len(tree.input.C11[i])>0):
        processors=tree.input.C11[i][1]
        if (not (str(currentNode.values[i]) in processors)):
            print( "Failed Type C.1.1 ",currentNode.values[i])
            return False

    if(len(tree.input.C12[i])>0):
        processors=tree.input.C12[i][1]
        if ((str(currentNode.values[i]) in processors)):
            print( "Failed Type C.1.2 ",currentNode.values[i])
            return False

    if (len(tree.input.C21)>0):
         for value in tree.input.C21:
             # [[1,2],x]
             #print (value)
             task_1=int(value[0][0])
             task_2=int(value[0][1])
             pross_ok=int(value[1])
             print(pross_ok)
             if(task_1-1 <len(currentNode.values) and (task_2-1 <len(currentNode.values))):
                 if(currentNode.values[task_1-1]!=currentNode.values[task_2-1]):
                     return False
                 elif(currentNode.values[task_1-1]!=pross_ok):
                     print( "Failed Type C.2.1 ",currentNode.values[i])
                     return False


    if (len(tree.input.C22)>0):
         for value in tree.input.C22:
             # [[1,2],x]
             #print (value)
             task_1=int(value[0][0])
             task_2=int(value[0][1])
             pross_bad=int(value[1])
             if(task_1-1 <len(currentNode.values) and (task_2-1 <len(currentNode.values))):
                 if(currentNode.values[task_1-1]==currentNode.values[task_2-1]):
                     return False
                 elif(currentNode.values[task_1-1]==pross_bad):
                     print( "Failed Type C.2.2 ",currentNode.values[i])
                     return False

    if (len(tree.input.C23)>0):
        #print (tree.input.C23)
        for value in tree.input.C23:
            # [[1,2],x]
            #print (value)
            task_1=int(value[0][0])
            task_2=int(value[1][0])
            pross_1=int(value[0][1])
            pross_2=int(value[1][1])

            #print ("Task #",task_1, " Pross OK #", pross_1," ")
            #print ("Task #",task_2, " Pross BAD #", pross_2," ")
            if(task_1-1 <len(currentNode.values) and (task_2-1 <len(currentNode.values))):
                if((currentNode.values[task_1-1]==pross_1) and (currentNode.values[task_2-1]==pross_2)):
                    print( "Failed Type C.2.3 ",currentNode.values[i])
                    return False



    return True

if __name__ == '__main__':
    # print(tree._in.N)
    # print(tree._in.M)
    # print(tree._in.L)
    # print(tree._in.deadline)
    #
    #print ("Tipo 1 1",tree.input.C11)
    # print ("Tipo 1 2",tree._in.C12)
    # print ("Tipo 2 1",tree._in.C21)
    # print ("Tipo 2 2",tree._in.C22)
    # print ("Tipo 2 3",tree._in.C23)

    root = tree.makeGraph(int(tree.input.N), int(tree.input.M), tree.input.L)
    DFSAlg(root)
    #print( root.values )
    #DFSAlg(root)
