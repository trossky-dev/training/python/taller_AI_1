import fileinput

myFile=[]
global N
N=0
global M
M=0
global L
L=[]
global deadline
deadline=0
C11=[]
C12=[]
constraints={}
C21=[]
C22=[]
C23=[]

def loadInputs(i,file):
    if i==2:
        global N
        N=file[i-1]
    if i==3:
        global M
        M=file[i-1]
    if i==4:
        global L
        L=file[i-1].split(',')
    if i==5:
        global deadline
        deadline=file[i-1]

def loadConstraints1(i,file):
    file[i-1]=''.join(file[i-1].split())
    if(file[i-1].find("+")>-1):
        aux_1=file[i-1].split('+')
        aux_2=aux_1[1].split(',')
        global C11
        C11.append([aux_1[0],aux_2])


    elif(file[i-1].find("-")>-1):
        aux_1=file[i-1].split('-')
        aux_2=aux_1[1].split(',')
        global C12
        C12.append([aux_1[0],aux_2])
    else:
        pass

def loadConstraints2(i,file):
    file[i-1]=''.join(file[i-1].split())
    if(file[i-1].find("+")>-1):
        aux_2=file[i-1].split('+')
        aux_1=aux_2[0].split(',')
        global C21
        C21.append([aux_1,aux_2[1]])
    elif(file[i-1].find("-")>-1):
        aux_2=file[i-1].split('-')
        aux_1=aux_2[0].split(',')
        global C22
        C22.append([aux_1,aux_2[1]])
    elif(file[i-1].find("!")>-1):
        aux_2=file[i-1].split('!')
        aux_1=aux_2[0].split(',')
        aux_2=aux_2[1].split(',')
        global C23
        C23.append([[aux_1[0],aux_2[0]],[aux_1[1],aux_2[1]]])
    pass

def process(file,counter):
    for i in range(1,int(counter[3])+1,1):
        #Read Inputs
        if(counter[1]-counter[0]!=5):
            print ("Input parameters are failing")
            return
        if i> counter[0] and i< counter[1]:
            loadInputs(i,file)
        # Read constraints Unarias
        if i> counter[1] and i< counter[2]:
            loadConstraints1(i,file)
        # Read constraints Binarias
        if i> counter[2] and i<= counter[3]:
            loadConstraints2(i,file)



def readFile():
    for line in fileinput.input():
        line=line.rstrip('\n')
        if line.rstrip('\n'):
            myFile.append(line)
            if(line=='input'):
                inputRows=len(myFile)
                #print ("Entradas ",inputRows)

            elif(line=="constraints1"):
                c1Rows=len(myFile)
                #print ("Recctricciones Unarias",c1Rows)

            elif(line=="constraints2"):
                c2Rows=len(myFile)
                #print ("Recctricciones Binarias ",c2Rows)
    return int(inputRows),int(c1Rows),int(c2Rows),len(myFile)

process(myFile,readFile())
