import input
import Queue as q

class Node:
    def __init__(self, parent, values):
        self.values = values # Conjunto de valores que conforman una posible estado {T1,T2,T3,T4,...,Tn}
        self.parent = parent
        self.children = []
        self.procesorsTime = []

    def addTime(self, p, time):
        self.procesorsTime[p] += time


    def maxTime(self):
        return max(self.procesorsTime)

def makeGraph(tasks,processors, li):

    # T1, T2, T3, T4
    queue = q.Queue()
    #print(type(processors))
    times = [0] * (int(processors))
    root = Node(None, [])
    root.procesorsTime = times[:]
    queue.put(root)

    while not queue.empty():
        currentNode = queue.get() #Toma el primer elemento y lo elimina de la fila

        if( len(currentNode.values) <tasks ):
            for i in range(processors): # Se crean p nuevos hijos para un nodo padre
                newValues = currentNode.values[:]
                newValues.append(i+1)
                newChild = Node(currentNode, newValues)
                newChild.procesorsTime = currentNode.procesorsTime[:]
                newChild.addTime(i, int(li[len(newValues)-1]))
                currentNode.children.append(newChild)
                queue.put(newChild)
                #print("Father -> ",currentNode.values ," Child ", newChild.values)

    return root
